FROM openjdk:8-jre-stretch

RUN apt-get update && apt-get install git -y

COPY target/bestanden-api-1.0.0-SNAPSHOT.jar bestanden-api-1.0.0-SNAPSHOT.jar

ENTRYPOINT ["java", "-jar", "bestanden-api-1.0.0-SNAPSHOT.jar"]
