package nl.robertvankammen.bestandenapi.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

@RestController
public class BestandController {

    @PostMapping("/bestanden")
    public String uploadBestand(@RequestParam("bestand") MultipartFile bestand, @RequestParam("naam") String naam) throws IOException {
        System.out.println(LocalDateTime.now() + " naam: " + naam + " bestand: " + bestand.getOriginalFilename());
        bestand.transferTo(ophalenBestandNaam(bestand, naam));
        return "{\"status\": \"OK\"}";
    }

    private File ophalenBestandNaam(MultipartFile bestand, String naam) {
        final String filePath = "/bestanden/" + naam + "/" + bestand.getOriginalFilename();
        int count = 1;
        File file = new File(filePath);
        file.getParentFile().mkdirs();
        while (file.exists()) {
            file = new File("/bestanden/" + naam + "/" + count + bestand.getOriginalFilename());
            count++;
        }

        return file;
    }
}
