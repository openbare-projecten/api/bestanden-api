package nl.robertvankammen.bestandenapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestandenApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BestandenApiApplication.class, args);
    }

}

